<?php

namespace Addiesaas\Core\Support\Composer\Installer;

use Composer\Composer;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;

/**
 * Class AdopterPlugin
 *
 * @author  AddieDigital contact@addiedigital.com
 */
class AdopterPlugin implements PluginInterface
{
    /**
     * @param Composer $composer
     * @param IOInterface $io
     */
    public function activate(Composer $composer, IOInterface $io)
    {
        $installer = new AdopterInstaller($io, $composer);
        $composer->getInstallationManager()->addInstaller($installer);
    }
}
