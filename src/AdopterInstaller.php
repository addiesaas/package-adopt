<?php

namespace Addiesaas\Core\Support\Composer\Installer;

use Composer\Package\PackageInterface;
use Composer\Installer\LibraryInstaller;

/**
 * Class AdopterInstaller
 *
 * @author  AddieDigital contact@addiedigital.com
 */
class AdopterInstaller extends LibraryInstaller
{
    /**
     * Constant representing Directory Separator
     *
     * @var string
     */
    const DS = DIRECTORY_SEPARATOR;

    /**
     * Constant representing allowed package types defined in `type`
     * property of `composer.json`
     *
     * @var string
     */
    const PACKAGE_TYPES = [
        'addiesaas-container' => 'app/Containers',
        'addiesaas-ship' => 'app/Ship',
        'addiesaas-core' => 'app/Core',
        'addiesaas-nucleus' => 'app/Nucleus',
        'addiesaas-boot' => 'app/Boot',
        'addiesaas-tests' => 'tests',
    ];

    /**
     * {@inheritDoc}
     */
    public function getInstallPath(PackageInterface $package)
    {

        $prettyName = $package->getPrettyName();
        $packageType = $package->getType();
        $installPath = "vendor" . self::DS . $prettyName;
        $extras = json_decode(json_encode($package->getExtra()));

        if (isset($extras->addiesaas)) {
            $itemName = empty($extras->addiesaas->package->name) ?
                '' : $extras->addiesaas->package->name;
            $sectionName = empty($extras->addiesaas->package->section) ?
                '' : $extras->addiesaas->package->section;
            $itemInstallPath = empty($extras->addiesaas->package->installPath) ?
                '' : $extras->addiesaas->package->installPath;

            if (!empty($itemInstallPath)) {
                $installPath = $itemInstallPath;
            } elseif (!empty($itemName)) {
                $middlePath = self::PACKAGE_TYPES[$packageType];
                $installPath = implode(self::DS, [$middlePath, $sectionName, $itemName]);
                $installPath = preg_replace(['/\+/', '/\/+/'], ['\\', '/'], $installPath);
            }
        }

        return $installPath;
    }

    /**
     * {@inheritDoc}
     */
    public function supports($packageType)
    {
        $isSupported = in_array($packageType, array_keys(self::PACKAGE_TYPES));
        return $isSupported;
    }
}
